package it.unibo.oop.lab.reactivegui02;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;



/**
 * 
 *
 */
public class ConcurrentGUI extends JFrame {

    private static final long serialVersionUID = 1L;
    private static final double WIDTH_PERC = 0.2;
    private static final double HEIGHT_PERC = 0.1;
    private static final int SLEEP_AMOUNT = 100;
    private final JLabel counter = new JLabel();
    private final JButton up = new JButton("Up");
    private final JButton down = new JButton("Down");
    private final JButton stop = new JButton("Stop");

    ConcurrentGUI() {
        super();
        final Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        this.setSize((int) (dimension.getWidth() * WIDTH_PERC), (int) (dimension.getHeight() * HEIGHT_PERC));
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        final JPanel panel = new JPanel();
        this.getContentPane().add(panel);
        panel.add(counter);
        panel.add(up);
        panel.add(down);
        panel.add(stop);
        this.setVisible(true);

        final Agent agent = new Agent();
        new Thread(agent).start();
        stop.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                agent.stopCounting();
                up.setEnabled(false);
                down.setEnabled(false);
                stop.setEnabled(false);
            }
        });

        down.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                agent.setDown();
            }
        });
        up.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                agent.setUp();
            }
        });

    }

    /**
     * an agent for a concurrent UI bi-counter.
     *
     */
    public class Agent implements Runnable {

        private int counter = 0;
        private volatile boolean stop = false;
        private volatile boolean up = true;

        public void run() {
            while (!this.stop) {
                try {
                    SwingUtilities.invokeAndWait(new Runnable() {
                        public void run() {
                            ConcurrentGUI.this.counter.setText(Integer.toString(Agent.this.counter));
                        }
                    });
                  counter += (up ? +1 : -1);
                  Thread.sleep(SLEEP_AMOUNT);
                } catch (InvocationTargetException | InterruptedException ex) {
                    ex.printStackTrace();
                }
            }

        } 

        /**
         * Stops the counter.
         */
        public void stopCounting() {
            this.stop = true;
        }

        /**
         * Makes the counter incremental.
         */
        public void setUp() {
            this.up = true;
        }

        /**
         * Makes the counter decremental.
         */
        public void setDown() {
            this.up = false;
        }
    }
}
