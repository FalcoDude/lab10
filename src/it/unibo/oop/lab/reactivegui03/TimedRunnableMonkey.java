package it.unibo.oop.lab.reactivegui03;


/**
 * A timed actor executes the specified Runnable after sleep milliseconds.
 *
 */
public class TimedRunnableMonkey implements Runnable {
    private final int sleep;
    private final Runnable runnable;

    /**
     * @param sleep the time before the action is taken.
     * @param runnable the runnable to be executed.
     */
    public TimedRunnableMonkey(final int sleep, final Runnable runnable) {
        this.sleep = sleep;
        this.runnable = runnable;
    }

    /**
     * 
     */
    public void run() {
        try {
            Thread.sleep(sleep);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
        runnable.run();
    }

}