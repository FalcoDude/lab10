package testing;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {
 @Override
 public void start(final Stage stage)throws Exception {
 Group root = new Group();
 Scene scene = new Scene(root, 500, 300);

 stage.setTitle(" JavaFX Demo ");
 stage.setScene(scene);
 stage.show();
 }

 public static void main ( String [] args ) {
 launch ( args ) ;
 }
 }